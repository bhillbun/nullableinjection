﻿namespace Omu.ValueInjecter.Injections
{
    using System;
    using System.Reflection;

    public class NullableInjection : PropertyInjection
    {
        protected override void Execute(PropertyInfo sp, object source, object target)
        {
            var tp = target.GetType().GetProperty(sp.Name);

            if (tp == null) return;
            
            var spNullPropType = Nullable.GetUnderlyingType(sp.PropertyType);
            var tpNullPropType = Nullable.GetUnderlyingType(tp.PropertyType);

            if (sp.Name == tp.Name)
            {
                if (spNullPropType != null && tpNullPropType != null)
                {
                    if (spNullPropType.Name == tpNullPropType.Name)
                    {
                        tp.SetValue(target, sp.GetValue(source));
                    }
                }
                else if (spNullPropType == null && tpNullPropType != null)
                {
                    if (sp.PropertyType.Name == tpNullPropType.Name)
                    {
                        tp.SetValue(target, sp.GetValue(source));
                    }
                }
                else if (spNullPropType != null && tpNullPropType == null)
                {
                    if (spNullPropType.Name == tp.PropertyType.Name)
                    {
                        tp.SetValue(target, sp.GetValue(source));
                    }
                }
                else
                {
                    if (sp.PropertyType.Name == tp.PropertyType.Name)
                    {
                        tp.SetValue(target, sp.GetValue(source));
                    }
                }
            }
        }
    }
}