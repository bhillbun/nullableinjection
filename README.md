# README #

*NullableInjection* is a useful *ValueInjecter* extension that can be used to copy properties from one model to another regardless of whether they are Nullable or not.

Please visit *ValueInjecter's* GitHub website at [https://github.com/omuleanu/ValueInjecter](https://github.com/omuleanu/ValueInjecter) to learn how to use *ValueInjecter* or develop your own Injections.

### How do I get it? ###

You can download the source [here](https://bitbucket.org/bhillbun/nullableinjection/src/30796f45327e2647106187eed014139606b394f0/NullableInjection.cs?at=master)

### How do I use it? ###

*NullableInjection* uses the *ValueInjecter* namespace of *Omu.ValueInjecter.Injections* to make it easier to use without requiring extra **using** statements.

```
#!c#
public class MyModel {
    public int Id { get; set; }
    public string Name { get; set; }
}

public class MyViewModel {
    public int? Id { get; set; }
    public string Name { get; set; }
}

var model = new MyModel() {
    Id = 1,
    Name = "Test Model"
};

var viewModel = new MyViewModel();

viewModel.InjectFrom<NullableInjection>(model);

```